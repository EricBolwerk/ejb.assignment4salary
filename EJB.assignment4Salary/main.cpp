#include <iostream>
#include <conio.h>
#include <string>

struct salary
{
	int id;
	std::string firstName;
	std::string lastName;
	double payRate;
	int hours;
};

int main()
{
	int numWorkers = 0;

	std::cout << "How many Employees to you have? ";
	std::cin >> numWorkers;

	salary *Salary = new salary[numWorkers];

	for (int i = 0; i < numWorkers; i++)
	{
		std::cout << "Enter worker number " << i + 1 << "'s id number ";
		std::cin >> Salary[i].id;

		std::cout << "Enter worker number " << i + 1 << "'s first name ";
		std::cin >> Salary[i].firstName;

		std::cout << "Enter worker number " << i + 1 << "'s last name ";
		std::cin >> Salary[i].lastName;

		std::cout << "Enter worker number " << i + 1 << "'s pay rate ";
		std::cin >> Salary[i].payRate;

		std::cout << "Enter worker number " << i + 1 << "'s hours ";
		std::cin >> Salary[i].hours;
	}

	std::cout << "\nID\t\tName\t\tGross pay\n";

	for (int i = 0; i < numWorkers; i++)
	{
		double grossPay;
		grossPay = Salary[i].payRate * Salary[i].hours;

		std::cout << Salary[i].id << "\t\t" <<Salary[i].firstName << " " << Salary[i].lastName << "\t\t" << grossPay << "\n";
	}
	
	_getch();
	return(0);
}